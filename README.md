# osbb-zuul-app

_Project Name:  

“osbb-zuul-app” (Internal Project) 

Version: v.1.0

“Osbb-zuul-app” is a gateway service for “osbb-app” application.

_Getting Started:

-	Clone project

-	Start OsbbZullApplication

_Prerequisites:

Java:  8 Update 231

Maven:  3.6.2

Spring:  2.1.3

Zuul: 2.0.3

Eureka 2.1.3

Spring-cloud version:  Greenwich.SR4

Elasticsearch:  7.3.2

MySQL:   8

Encoding:  UTF-8



