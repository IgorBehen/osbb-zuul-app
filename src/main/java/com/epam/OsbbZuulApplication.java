package com.epam;

import com.epam.filters.ErrorFilter;
import com.epam.filters.PostFilter;
import com.epam.filters.PreFilter;
import com.epam.filters.RouteFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class OsbbZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(OsbbZuulApplication.class, args);
    }

    @Bean
    public PreFilter simpleFilter() {
        return new PreFilter();
    }

    @Bean
    public PostFilter postFilter() {
        return new PostFilter();
    }

    @Bean
    public ErrorFilter errorFilter() {
        return new ErrorFilter();
    }

    @Bean
    public RouteFilter routeFilter() {
        return new RouteFilter();
    }
}
